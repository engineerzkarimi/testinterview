﻿using System.Collections.Generic;
using System;
using TestInterview.Api.Contracts.Interfaces;
using Entities = TestInterview.Api.Contracts.Entities;
using System.Threading.Tasks;
using TestInterview.Api.Contracts.Models;
using System.Linq;

namespace TestInterview.Api.BLL
{
    public class CustomerBS : ICustomerBS
    {
        private ICustomerRepository _CustomerRepository;
        
        public CustomerBS(ICustomerRepository CustomerRepository)
        {
            if (CustomerRepository != null)
                this._CustomerRepository = CustomerRepository;            
        }
        
        public async Task<IList<Entities.Customer>> GetCustomers()
        {
            return await this._CustomerRepository.GetCustomers();
        }

        public async Task<Entities.Customer> GetCustomerById(int id)
        {
            return await this._CustomerRepository.GetCustomerById(id);
        }

        public async Task<Entities.Customer> GetCustomerByIdentifier(CustomerRequests CustomerRequestsIns)
        {
            System.Linq.Expressions.Expression<System.Func<Entities.Customer, bool>> expCustomer
                = e => e.CustomerId >0;

            if (CustomerRequestsIns.CustomerID == 0 && CustomerRequestsIns.Email == "")
            { return null; }
            if (CustomerRequestsIns.Senario == 1)
            {

                expCustomer
                     = e => e.CustomerId == CustomerRequestsIns.CustomerID;
              
            }

           else if (CustomerRequestsIns.Senario == 2)
            {

                expCustomer
                     = e => e.Email == CustomerRequestsIns.Email;

            }
            else if(CustomerRequestsIns.Senario == 3)
            {

                 expCustomer
                     = e => (e.Email == CustomerRequestsIns.Email && e.CustomerId == CustomerRequestsIns.CustomerID);


            }
            return await this._CustomerRepository.GetCustomerByIdentifier(expCustomer);


        }
    }
}
