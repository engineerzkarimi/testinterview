﻿using System.Collections.Generic;
using System;
using System.Linq;
using TestInterview.Api.Common;
using System.Threading.Tasks;
using TestInterview.Api.Contracts.Interfaces;
using Entities = TestInterview.Api.Contracts.Entities;
using Models = TestInterview.Api.Contracts.Models;
using TestInterview.Api.Contracts.Models;

namespace TestInterview.Api.BLL
{
    public class TransactionBS : ITransactionBS
    {
        private ITransactionRepository _TransactionRepository;
        
        public TransactionBS(ITransactionRepository TransactionRepository)
        {
            if (TransactionRepository != null)
                this._TransactionRepository = TransactionRepository;            
        }
        
        public async Task<IList<Entities.Transaction>> GetTransactions()
        {
            return await this._TransactionRepository.GetTransactions();
        }

        public async Task<Entities.Transaction> GetTransactionById(int id)
        {
            return await this._TransactionRepository.GetTransactionById(id);
        }

        public async Task<IList<Entities.Transaction>> GetTransactionByCustomerId(CustomerTransactionSearchFilter CustomerTransactionIns)
        {
            IList<Entities.Transaction > lstTransResult= new List<Entities.Transaction>();
            System.Linq.Expressions.Expression<System.Func<Entities.Transaction, bool>> expCustomer
               = e => e.TransactionId > 0;

            if (CustomerTransactionIns.CustomerID == 0 )
            { return null; }
            else
            {
                expCustomer
                     = e => e.Customer.CustomerId == CustomerTransactionIns.CustomerID;
            }


            if ( CustomerTransactionIns.Senario == 1)
            {

                return null;

            }

            if ( CustomerTransactionIns.Senario == 2)
            {
               
                lstTransResult.Add( await this._TransactionRepository.FindAsync(expCustomer));
                return lstTransResult;

            }
            if ( CustomerTransactionIns.Senario == 3)
            {
              

                return await this._TransactionRepository.FindAllAsync(expCustomer);


            }
            return null;
        }
      


    }
}
