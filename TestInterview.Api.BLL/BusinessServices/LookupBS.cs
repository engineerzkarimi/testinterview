﻿using System.Collections.Generic;
using System;
using System.Linq;
using TestInterview.Api.Contracts.Interfaces;
using Entities = TestInterview.Api.Contracts.Entities;
using Models = TestInterview.Api.Contracts.Models;
using TestInterview.Api.DAL;

namespace TestInterview.Api.BLL
{
    public class LookupBS : ILookupBS
    {
        //Instantiate from the ITestInterviewLookupRepository 
        private ITestInterviewLookupRepository<Entities.Category> _categoryRepository;
        private ITestInterviewLookupRepository<Entities.TransactionStatusType> _TransactionStatusTypeRepository;
        
        public LookupBS(ITestInterviewLookupRepository<Entities.Category> cateoryRepository,
                        ITestInterviewLookupRepository<Entities.TransactionStatusType> TransactionStatusTypeRepository)
        {
            this._categoryRepository = cateoryRepository;
            this._TransactionStatusTypeRepository = TransactionStatusTypeRepository;            
        }

        public IList<Models.Category> LookupCategories()
        {
            var query = this._categoryRepository.GetIQueryable();
            var list = query.Select(a => new Models.Category
            {
                CategoryId = a.CategoryId,
                CategoryName = a.CategoryName
            });

            return list.ToList();
        }

        public IList<Models.TransactionStatusType> LookupTransactionStatusTypes()
        {
            var query = this._TransactionStatusTypeRepository.GetIQueryable();
            var list = query.Select(a => new Models.TransactionStatusType
            {
                StatusCode = a.StatusCode,
                Description = a.Description
            });

            return list.ToList();
        }        
    }
}

