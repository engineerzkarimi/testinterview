﻿using System;
using System.Collections.Generic;

namespace TestInterview.Api.Contracts.Models
{
    public class CustomerRequests
    {
        public string Email { get; set; }
        public int CustomerID { get; set; }
        public int Senario { get; set; }
        
    }

    public class SaveCustomerRequest
    {
        public Customer Customer { get; set; }
    }
}