using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TestInterview.Api.Contracts.Models
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class Transaction
    {
        [JsonProperty(PropertyName = "TransactionId")]        
        public int TransactionId { get; set; }

        [JsonProperty(PropertyName = "TransactionDate")]
        public DateTime TransactionDate { get; set; }

        [JsonProperty(PropertyName = "Amount")]
        public Nullable<decimal> Amount { get; set; }

        [JsonProperty(PropertyName = "CurrencyCode")]
        public string CurrencyCode { get; set; }


        public int TransactionStatus { get; set; }

        [JsonProperty(PropertyName = "Status")]
        public string Status { get; set; }
    }
}
