﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestInterview.Api.Contracts.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public Decimal MobileNo { get; set; }
        public string Email { get; set; }

        public IList<Transaction> Transactions { get; set; }
    }
}
