﻿using System;
using System.Collections.Generic;

namespace TestInterview.Api.Contracts.Models
{
    public class TransactionResponse 
    {
        public Transaction Transaction { get; set; }
    }

     
    public class TransactionListResponse 
    {
        public Transactions Transactions { get; set; }
        public int TotalCount { get; set; }
        public int newPageIndex { get; set; }
    }

    public class Transactions : List<Transaction> { }

   
}
