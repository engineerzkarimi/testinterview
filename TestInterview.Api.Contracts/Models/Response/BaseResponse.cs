﻿
namespace TestInterview.Api.Contracts.Models
{
    using System;

    public class BaseResponse
    {
        public Status Status { get; set; }
    }

}
