﻿using System;
using System.Collections.Generic;

namespace TestInterview.Api.Contracts.Models
{ 
    public class CustomerListResponse
    {
        public Customers Customers { get; set; }
    }

    public class CustomerTransactionListResponse
    {
        public Customers Customers { get; set; }
        public IList<Transaction> Transactions { get; set; }
    }

    public class Customers : List<Customer> { }



}
