﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TestInterview.Api.Contracts.Interfaces
{
    public interface ITransactionRepository : IGenericRepository<Entities.Transaction>
    {
        Task<IList<Entities.Transaction>> GetTransactions();
        Task<Entities.Transaction> GetTransactionById(int id);

        Task<IList<Entities.Transaction>> GetTransactionByCustomerId(Expression<Func<Entities.Transaction, bool>> transactionMatch);
    }
}
