﻿using System;
using System.Collections.Generic;

namespace TestInterview.Api.Contracts.Interfaces
{
    public interface ITestInterviewLookupRepository<TEntity> : IGenericRepository<TEntity>  where TEntity : class
    {        
    }
}
