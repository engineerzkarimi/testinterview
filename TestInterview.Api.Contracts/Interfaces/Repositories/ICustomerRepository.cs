﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TestInterview.Api.Contracts.Models;

namespace TestInterview.Api.Contracts.Interfaces
{
    public interface ICustomerRepository : IGenericRepository<Entities.Customer>
    {
        Task<IList<Entities.Customer>> GetCustomers();
        Task<Entities.Customer> GetCustomerById(int id);
        Task<Entities.Customer> GetCustomerByIdentifier(Expression<Func<Entities.Customer, bool>> customerMatch);
        Task<int> AddCustomer(Entities.Customer inputEt);
    }
}
