﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using TestInterview.Api.Contracts.Models;

namespace TestInterview.Api.Contracts.Interfaces
{
    public interface ICustomerBS
    {
        Task<IList<Entities.Customer>> GetCustomers();
        Task<Entities.Customer> GetCustomerById(int id);
        Task<Entities.Customer> GetCustomerByIdentifier(CustomerRequests CustomerRequestsIns);
      //  Task<int> AddCustomer(Entities.Customer inputEt);

    }

}