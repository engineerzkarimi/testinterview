﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using TestInterview.Api.Contracts.Models;

namespace TestInterview.Api.Contracts.Interfaces
{
    public interface ITransactionBS
    {
        Task<IList<Entities.Transaction>> GetTransactions();
        Task<Entities.Transaction> GetTransactionById(int id);

        Task<IList<Entities.Transaction>> GetTransactionByCustomerId(CustomerTransactionSearchFilter CustomerTransactionSearchFilterIns);
    }

}