﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestInterview.Api.Contracts.Entities
{
    public class Transaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long TransactionId { get; set; }
        public DateTime  TransactionDate { get; set; }
        public Decimal?  Amount { get; set; }
        public string  CurrencyCode { get; set; }
        public virtual int TransactionStatus { get; set; }

    public virtual Customer Customer { get; set; }
      
    }    
}
