﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestInterview.Api.Contracts.Entities
{
    public class TransactionStatusType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StatusCode { get; set; }
        public string Description { get; set; }
        
    }
}
