﻿using Microsoft.EntityFrameworkCore;
using TestInterview.Api.Contracts.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace TestInterview.Api.DAL
{
    public class TestInterviewDataContext : DbContext
    {
        //public string ConnectionString { get; set; }

        public TestInterviewDataContext(DbContextOptions<TestInterviewDataContext> options) : base(options)
        {
        
        }

        public DbSet<Transaction> Transactions { get; set; }
      
        public DbSet<TransactionStatusType> TransactionStatusTypes { get; set; }
        public DbSet<Customer> Customers { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            //base.OnModelCreating(builder);
            builder.Entity<Transaction>().ToTable("Transaction");
          
            builder.Entity<TransactionStatusType>().ToTable("TransactionStatusType");
            builder.Entity<Customer>().ToTable("Customer");
            base.OnModelCreating(builder);
        }

      
    }
}