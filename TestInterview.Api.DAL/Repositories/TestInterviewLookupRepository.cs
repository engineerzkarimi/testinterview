﻿using System;
using System.Linq;
using System.Collections.Generic;
using TestInterview.Api.Common;
using Entities = TestInterview.Api.Contracts.Entities;
using Models = TestInterview.Api.Contracts.Models;
using TestInterview.Api.Contracts.Interfaces;
using Microsoft.EntityFrameworkCore;


namespace TestInterview.Api.DAL
{
    public class TestInterviewLookupRepository<TEntity> : GenericRepository<TEntity>, ITestInterviewLookupRepository<TEntity> where TEntity : class
    {
        //Just need to pass db context to GenericRepository. 
        public TestInterviewLookupRepository(TestInterviewDataContext context)
            : base(context)
        {            
        }
        
    }
}
