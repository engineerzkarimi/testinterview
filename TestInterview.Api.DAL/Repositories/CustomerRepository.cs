﻿using System;
using System.Linq;
using System.Collections.Generic;
using TestInterview.Api.Common;
using Entities = TestInterview.Api.Contracts.Entities;
using Models = TestInterview.Api.Contracts.Models;
using TestInterview.Api.Contracts.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace TestInterview.Api.DAL
{
    public class CustomerRepository : GenericRepository<Entities.Customer>, ICustomerRepository
    {
        private TestInterviewDataContext testInterviewDataContext;
        public CustomerRepository(TestInterviewDataContext context)
            : base(context)
        {
            testInterviewDataContext = context;
        }

        public async Task<IList<Entities.Customer>> GetCustomers()
        {
            return await this.GetAllAsync();
        }

        public async Task<Entities.Customer> GetCustomerById(int id)
        {
           
            return await this.GetByIdAsync(id);
        }
  

        public async Task<Entities.Customer> GetCustomerByIdentifier(Expression<Func<Entities.Customer, bool>> customerMatch)
        {
           
            return await this.FindAsync(customerMatch);
        }

        public async Task<int> AddCustomer(Entities.Customer inputEt)
        {
            inputEt.CustomerId = 0;
           
            await this.InsertAsync(inputEt, true);
            //this.Commit();
            return inputEt.CustomerId;
        }

    }
}
