﻿using System;
using System.Linq;
using System.Collections.Generic;
using TestInterview.Api.DAL;
using TestInterview.Api.Common;
using Entities = TestInterview.Api.Contracts.Entities;
using Models = TestInterview.Api.Contracts.Models;
using TestInterview.Api.Contracts.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace TestInterview.Api.DAL
{
    public class TransactionRepository : GenericRepository<Entities.Transaction>, ITransactionRepository
    {
        private TestInterviewDataContext testInterviewDataContext;
        public TransactionRepository(TestInterviewDataContext context)
            : base(context)
        {
            testInterviewDataContext = context;
        }

        public async Task<IList<Entities.Transaction>> GetTransactions()
        {
            return await this.GetAllAsync();
        }

        public async Task<Entities.Transaction> GetTransactionById(int id)
        {
            return await this.GetByIdAsync(id);
        }

        public async Task<IList<Entities.Transaction>> GetTransactionByCustomerId(Expression<Func<Entities.Transaction, bool>> transactionMatch)
        {
            return await this.FindAllAsync(transactionMatch);
        }


    }
}
