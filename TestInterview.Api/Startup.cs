﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;
using TestInterview.Api.BLL;
using TestInterview.Api.Common;
using TestInterview.Api.Contracts.Configurations;
using TestInterview.Api.Contracts.Interfaces;
using TestInterview.Api.DAL;

namespace TestInterview.Api
{
    public class Startup
    {


        readonly IHostingEnvironment HostingEnvironment;
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment env)
        {
            HostingEnvironment = env;

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            AutoMapper.Mapper.Initialize(cfg => {
                cfg.CreateMap<Contracts.Entities.Transaction, Contracts.Models.Transaction>()
                    .ForMember(dest => dest.Status,
                       m => m.MapFrom(a => a.TransactionStatus == 1 ? "Confirm" : (a.TransactionStatus == 2 ? "Cancled" : "Failed")));

            });
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1).AddJsonOptions(opt =>
            {
                var resolver = opt.SerializerSettings.ContractResolver;
                if (resolver != null)
                {
                    var res = resolver as DefaultContractResolver;
                    res.NamingStrategy = null;
                }
            }); 

            

            //Also make top level configuration available (for EF configuration and access to connection string)
            services.AddSingleton(Configuration); //IConfigurationRoot
            services.AddSingleton<IConfiguration>(Configuration);

            //Add Support for strongly typed Configuration and map to class
            services.AddOptions();
            services.Configure<AppConfig>(Configuration.GetSection("AppConfig"));

            //Set database.
            if (Configuration["AppConfig:UseInMemoryDatabase"] == "true")
            {
                services.AddDbContext<TestInterviewDataContext>(opt => opt.UseInMemoryDatabase("TestInterviewDbMemory"));
            }
            else
            {
                services.AddDbContext<TestInterviewDataContext>(c =>
                    c.UseSqlServer(Configuration.GetConnectionString("TestInterviewDbConnection")));
            }

            //Cors policy is added to controllers via [EnableCors("CorsPolicy")]
            //or .UseCors("CorsPolicy") globally
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            //Instance injection
            services.AddScoped(typeof(IAutoMapConverter<,>), typeof(AutoMapConverter<,>));
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped(typeof(ITestInterviewLookupRepository<>), typeof(TestInterviewLookupRepository<>));
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();

            services.AddScoped<ITransactionBS, TransactionBS>();
            services.AddScoped<ICustomerBS, CustomerBS>();


            //AutoMapper.Mapper.Initialize(cfg => {
            //    cfg.CreateMap<Contracts.Entities.Transaction, Contracts.Models.Transaction>()
            //        .ForMember(dest => dest.Status,
            //           m => m.MapFrom(a => a.TransactionStatus == 1 ? "Confirm" : (a.TransactionStatus == 2 ? "Cancled" : "Failed")));

            //});
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });

           

          
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
         IHostingEnvironment env,
         ILoggerFactory loggerFactory,
         IConfiguration configuration)
        {


            // Serilog config
            Log.Logger = new LoggerConfiguration()
                    .WriteTo.RollingFile(pathFormat: "logs\\log-{Date}.log")
                    .CreateLogger();

            if (env.IsDevelopment())
            {
                loggerFactory
                    .AddDebug()
                    .AddConsole()
                    .AddSerilog();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                loggerFactory
                    .AddSerilog();
                app.UseExceptionHandler(errorApp =>

                    //Application level exception handler here - this is just a place holder
                    errorApp.Run(async (context) =>
                    {
                        context.Response.StatusCode = 500;
                        context.Response.ContentType = "text/html";
                        await context.Response.WriteAsync("<html><body>\r\n");
                        await context.Response.WriteAsync(
                                "We're sorry, we encountered an un-expected issue with your application.<br>\r\n");

                        //Capture the exception
                        var error = context.Features.Get<IExceptionHandlerFeature>();
                        if (error != null)
                        {
                            //This error would not normally be exposed to the client
                            await context.Response.WriteAsync("<br>Error: " +
                                    HtmlEncoder.Default.Encode(error.Error.Message) + "<br>\r\n");
                        }
                        await context.Response.WriteAsync("<br><a href=\"/\">Home</a><br>\r\n");
                        await context.Response.WriteAsync("</body></html>\r\n");
                        await context.Response.WriteAsync(new string(' ', 512)); // Padding for IE
                    }));
            }

            app.UseDatabaseErrorPage();
            app.UseStatusCodePages();
            app.UseStaticFiles();



            //Apply CORS.
            app.UseCors("CorsPolicy");


            app.UseMvc();
            app.UseHttpsRedirection();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });


        }
    }
}
