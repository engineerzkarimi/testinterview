﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TestInterview.Api.Common;
using TestInterview.Api.Contracts.Interfaces;
using Entities = TestInterview.Api.Contracts.Entities;
using Models = TestInterview.Api.Contracts.Models;

namespace TestInterview.Api.Controllers
{
    [Route("api/Customers")]
    public class CustomersController : Controller
    {
        private ICustomerBS bs;
        private ITransactionBS ts;
        private IAutoMapConverter<Contracts.Entities.Customer, Models.Customer> mapEntityToModel;
        private IAutoMapConverter<Models.Customer, Entities.Customer> mapModelToEntity;

        private IAutoMapConverter<Contracts.Entities.Transaction, Models.Transaction> mapEntityToModel_ts;
        private IAutoMapConverter<Models.Transaction, Entities.Transaction> mapModelToEntity_ts;

        public CustomersController(ICustomerBS CustomerBS, ITransactionBS TransactionBS,
                                  IAutoMapConverter<Contracts.Entities.Customer, Models.Customer> convertEntityToModel,
                                  IAutoMapConverter<Models.Customer, Entities.Customer> convertModelToEntity,


                                    IAutoMapConverter<Contracts.Entities.Transaction, Models.Transaction> convertEntityToModel_ts,
                                  IAutoMapConverter<Models.Transaction, Entities.Transaction> convertModelToEntity_ts


            )
        {
            
            bs = CustomerBS;
            ts = TransactionBS;
            this.mapEntityToModel = convertEntityToModel;
            this.mapModelToEntity = convertModelToEntity;

            this.mapEntityToModel_ts = convertEntityToModel_ts;
            this.mapModelToEntity_ts = convertModelToEntity_ts;
        }

        [Route("~/api/getCustomerlist")]
        [HttpGet]
        public async Task<Models.CustomerListResponse> GetCustomerList()
        {
            var resp = new Models.CustomerListResponse();
            resp.Customers = new Contracts.Models.Customers();

            var rtnList = await bs.GetCustomers();
            var convtList = mapEntityToModel.ConvertObjectCollection(rtnList);
            resp.Customers.AddRange(convtList);
            return resp;
        }


        [Route("{id:int}", Name = "GetCustomerById")]
        [HttpGet]
        //[ResponseType(typeof(Customer))]
        public async Task<IActionResult> GetCustomerById(int id)
        {
            var eCustomer = await bs.GetCustomerById(id);
            if (eCustomer == null)
            {
                return NotFound();
            }
            else
            {
                Contracts.Models.Customer mCustomer = mapEntityToModel.ConvertObject(eCustomer);
                return Ok(mCustomer);
            }
        }

       [Route(  "GetCustomerInquery")]
        [HttpGet]
        //[ResponseType(typeof(Customer))]
        public async Task<IActionResult> GetCustomerByIdentifier(int customerId,string email)
        {
         email = email == null ? "" : email;
            #region Set Senario
            int userSenario = 0;
            if (customerId > 0 && email == "")
            {
                userSenario = 1;

            }

            if (customerId == 0 && email != "")
            {

                userSenario = 2;

            }
            if (customerId > 0 && email != "")
            {

                userSenario = 3;

            }
            #endregion
           
            var eCustomer = await bs.GetCustomerByIdentifier(new Models.CustomerRequests { CustomerID= customerId ,Email=email, Senario = userSenario });
            if (eCustomer == null)
            {
                return NotFound();
            }
            else
            {
            
                Contracts.Models.Customer mCustomer = mapEntityToModel.ConvertObject(eCustomer);
                mCustomer.Transactions = new List<Models.Transaction>();


                var eTransactionLst = await ts.GetTransactionByCustomerId(new Models.CustomerTransactionSearchFilter {
                    CustomerID = eCustomer.CustomerId ,Senario= userSenario});
                if (eTransactionLst != null)
                {
                    foreach (Entities.Transaction ins in eTransactionLst)
                    {

                  
                        mCustomer.Transactions.Add(mapEntityToModel_ts.ConvertObject(ins));

                    }
                }
             
                return Ok(mCustomer);
            }
        }




    }
}
